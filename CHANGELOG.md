# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] -- 2024-01-05
### Added
- Automatic environment variable handling
- Handle BooleanOptionalAction

## [0.2.0] -- 2024-01-04
### Added
- `parse_args` without arguments now reads `sys.argv[1:]`
- `add_argument`
- it is now possible to access undeclared parameters
- parameters and sections can now be accessed as attributes (getattr)
- ArgumentParser accepts root section name parameter

### Changed
- `read_args` renamed to `parse_args`
- all ArgumentParser constructor parameters are keyword-only now

### Removed
- `add_parameter` -- `add_argument` should be used instead

## [0.1.0] -- 2024-01-03
### Added
- this changelog
- readme
- license
- initial implementation of the ArgumentParser class

[Unreleased]: https://gitlab.com/aragaer/yacp/-/compare/v0.3.0...HEAD
[0.3.0]: https://gitlab.com/aragaer/yacp/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/aragaer/yacp/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/aragaer/yacp/-/tags/v0.1.0
