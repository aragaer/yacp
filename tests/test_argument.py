# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from decimal import Decimal
from os import environ

from yacp import ArgumentParser


class YacpArgumentTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()

    def test_add_argument(self):
        self._yacp.add_argument("--config-file", default="config.ini")

        self.assertEqual("config.ini", self._yacp["config_file"])

        self._yacp.parse_args(["--config-file", "other.ini"])

        self.assertEqual("other.ini", self._yacp["config_file"])

    def test_dont_add_argument(self):
        self._yacp.add_argument(dest="file", default="config.ini")

        self.assertEqual("config.ini", self._yacp["file"])

        self._yacp.parse_args(["--file", "other.ini"])

        self.assertEqual("config.ini", self._yacp["file"])

    def test_no_dest(self):
        with self.assertRaises(TypeError):
            self._yacp.add_argument(default="config.ini")

    def test_store_true_argument(self):
        self._yacp.add_argument("-x", "--flag", action='store_true')

        self.assertEqual(bool, type(self._yacp["flag"]))
        self.assertEqual(False, self._yacp["flag"])

        self._yacp.parse_args(["-x"])

        self.assertEqual(True, self._yacp["flag"])

    def test_store_false_argument(self):
        self._yacp.add_argument("-x", "--flag", action='store_false')

        self.assertEqual(True, self._yacp["flag"])

        self._yacp.parse_args(["-x"])

        self.assertEqual(False, self._yacp["flag"])

    def test_store_const_argument(self):
        self._yacp.add_argument("-x", "--flag", action='store_const', const=42)

        self.assertIsNone(self._yacp["flag"])

        self._yacp.parse_args(["-x"])

        self.assertEqual(42, self._yacp["flag"])

    def test_arg_env(self):
        environ["MY_FLAG"] = "42"

        self._yacp.add_argument("-x", "--flag", env="MY_FLAG", type=int)

        self.assertEqual(42, self._yacp["flag"])

        self._yacp.parse_args(["-x", "15"])

        self.assertEqual(15, self._yacp["flag"])

    def test_arg_file(self):
        section = self._yacp.add_section("api")
        section.add_argument("-v", "--verbose", dest="verbose_logs",
                             action='store_true')

        self._yacp.parse_args([])
        self._yacp.read_string("[api]\nverbose_logs = true")

        self.assertEqual(True, self._yacp["api"]["verbose_logs"])

    def test_arg_env_store(self):
        environ["VERBOSE_LOGS"] = "true"
        section = self._yacp.add_section("api")
        section.add_argument("-v", "--verbose", dest="verbose_logs",
                             env="VERBOSE_LOGS", action='store_true')

        self._yacp.parse_args([])

        self.assertEqual(True, self._yacp["api"]["verbose_logs"])

    def test_inv_arg(self):
        self._yacp.add_argument("--flag", action='store_true')
        self._yacp.add_argument("--no-flag", dest="flag", action='store_false')

        self._yacp.parse_args(["--flag"])
        self._yacp.read_string("flag = false")
        self.assertEqual(True, self._yacp["flag"])

        self._yacp.parse_args(["--no-flag"])
        self._yacp.read_string("flag = true")
        self.assertEqual(False, self._yacp["flag"])

    def test_multi_const_arg(self):
        self._yacp.add_argument("-a", dest="value", action='store_const', const=1)
        self._yacp.add_argument("-b", dest="value", action='store_const', const=2)

        self.assertIsNone(self._yacp["value"])

        self._yacp.parse_args(["-a"])
        self.assertEqual(1, self._yacp["value"])

        self._yacp.parse_args(["-b"])
        self.assertEqual(2, self._yacp["value"])

    def test_type_from_action(self):
        self._yacp.add_argument(dest="value", action='store_true')

        self.assertEqual(bool, type(self._yacp["value"]))
        self.assertEqual(False, self._yacp["value"])

    def test_type_from_const(self):
        self._yacp.add_argument(dest="value", action='store_const', const=10)

        self.assertIsNone(self._yacp["value"])

        self._yacp.read_string("value = 15")

        self.assertEqual(int, type(self._yacp["value"]))
        self.assertEqual(15, self._yacp["value"])


class YacpArgumentConversionTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser(converters={
            'weird': lambda x: 42 + int(x),
            'decimal': Decimal,
            'set': lambda s: set(x for x in map(str.strip, s.split(",")) if x),
        })

    def test_arg_conversion(self):
        self._yacp.add_argument("-x", "--flag", type='weird', default=3)

        self.assertEqual(45, self._yacp["flag"])

        self._yacp.read_string("flag = 10")
        self.assertEqual(52, self._yacp["flag"])

        self._yacp.parse_args(["-x", "15"])

        self.assertEqual(57, self._yacp["flag"])

    def test_arg_const_conversion(self):
        self._yacp.add_argument("-x", "--flag", type='weird',
                                action='store_const', const=3)

        self.assertIsNone(self._yacp["flag"])

        self._yacp.parse_args(["-x"])

        self.assertEqual(45, self._yacp["flag"])

    def test_arg_const_inferrence(self):
        self._yacp.add_argument("-x", "--flag", action="store_const", const=Decimal(42))

        self.assertIsNone(self._yacp["flag"])

        self._yacp.read_string("flag = 15")
        self.assertEqual(Decimal, type(self._yacp["flag"]))
        self.assertEqual(Decimal(15), self._yacp["flag"])

        self._yacp.parse_args(["-x"])

        self.assertEqual(Decimal, type(self._yacp["flag"]))
        self.assertEqual(Decimal(42), self._yacp["flag"])

    def test_default_inferrence(self):
        self._yacp.add_argument("-x", "--flag", default=Decimal(42))

        self.assertEqual(Decimal, type(self._yacp["flag"]))

    def test_capitalized_type(self):
        self._yacp.add_argument("-x", "--flag", type='Decimal')

        self._yacp.parse_args(["-x", "42"])

        self.assertEqual(Decimal, type(self._yacp.flag))

    def test_set(self):
        self._yacp.add_argument(dest="flag", type=set, default="")

        self.assertEqual(set, type(self._yacp.flag))
        self.assertEqual(set(), self._yacp.flag)

        self._yacp.read_string("flag = a, b, c")

        self.assertEqual(set, type(self._yacp.flag))
        self.assertEqual(set("abc"), self._yacp.flag)
