# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from argparse import BooleanOptionalAction

from yacp import ArgumentParser


class YacpActionTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()

    def test_boolean_option(self):
        self._yacp.add_argument("--flag", action=BooleanOptionalAction)

        self._yacp.read_string("flag = false")
        self.assertEqual(False, self._yacp["flag"])
        self._yacp.parse_args(["--flag"])
        self.assertEqual(True, self._yacp["flag"])

        self._yacp.parse_args([])
        self._yacp.read_string("flag = true")
        self.assertEqual(True, self._yacp["flag"])
        self._yacp.parse_args(["--no-flag"])
        self.assertEqual(False, self._yacp["flag"])
