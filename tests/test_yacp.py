# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from yacp import ArgumentParser


class YacpTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()

    def test_unset_argument(self):
        self._yacp.add_argument(dest="param")

        self.assertIsNone(self._yacp.param)

        self._yacp.read_string("param = yo")

        self.assertEqual(str, type(self._yacp.param))
        self.assertEqual("yo", self._yacp.param)

    def test_args_int_argument(self):
        self._yacp.add_argument("--test", dest="test_param", type=int)

        self._yacp.parse_args(["--test", "42"])
        self.assertEqual(self._yacp.test_param, 42)

    def test_args_default_value(self):
        self._yacp.add_argument("--test", dest="test_param", default=42, type=int)

        self._yacp.parse_args([])
        self.assertEqual(self._yacp["test_param"], 42)

    def test_args_bool_parameter(self):
        self._yacp.add_argument("--test", dest="test_param", action='store_true')

        self.assertFalse(self._yacp["test_param"], "before args")

        self._yacp.parse_args([])
        self.assertFalse(self._yacp["test_param"], "empty args")

        self._yacp.parse_args(["--test"])
        self.assertTrue(self._yacp["test_param"], "non-empty args")

    def test_file_parameter(self):
        self._yacp.add_argument(dest="test_param", action='store_true')

        self.assertFalse(self._yacp["test_param"])

        self._yacp.read_string("test_param=true")
        self.assertTrue(self._yacp["test_param"])

        self._yacp.read_string("test_param=false")
        self.assertFalse(self._yacp["test_param"])

    def test_section(self):
        section = self._yacp.add_section("stuff")
        section.add_argument(dest="test_param", type=bool)

        self.assertIsNone(self._yacp.stuff.test_param)

        self._yacp.read_string("[stuff]\ntest_param=true")
        self.assertTrue(self._yacp["stuff"]["test_param"])

    def test_read_override(self):
        self._yacp.add_argument(dest="test_param", type=int)

        self._yacp.read_string("test_param=1")
        self._yacp.read_string("test_param=2")
        self.assertEqual(2, self._yacp["test_param"])

    def test_arg_priority(self):
        self._yacp.add_argument("--test", dest="test_param", type=int)

        self._yacp.parse_args(["--test", "42"])
        self._yacp.read_string("test_param=15")

        self.assertEqual(42, self._yacp["test_param"])

    def test_undeclared_parameter(self):
        self._yacp.read_string("param = true")

        self.assertEqual(str, type(self._yacp["param"]))
        self.assertEqual('true', self._yacp["param"])

    def test_undeclared_section(self):
        self._yacp.read_string("[stuff]\nparam = true")

        self.assertEqual(str, type(self._yacp["stuff"]["param"]))
        self.assertEqual('true', self._yacp["stuff"]["param"])

    def test_declared_root_over_undeclared_section(self):
        self._yacp.add_argument("--stuff")

        self._yacp.read_string("[stuff]\nparam = true")

        self.assertIsNone(self._yacp["stuff"])

    def test_undeclared_section_over_undeclared_root(self):
        self._yacp.read_string("stuff = true\n[stuff]\nparam = true")

        self.assertNotEqual(str, type(self._yacp["stuff"]))
        self.assertEqual(str, type(self._yacp["stuff"]["param"]))
        self.assertEqual('true', self._yacp["stuff"]["param"])

    def test_late_declare(self):
        self._yacp.read_string("param = true")

        self.assertEqual(str, type(self._yacp.param))
        self.assertEqual('true', self._yacp.param)
        self._yacp.add_argument("--param", action='store_true')

        self.assertEqual(bool, type(self._yacp.param))
        self.assertTrue(self._yacp.param)

    def test_late_section(self):
        self._yacp.read_string("[stuff]\nparam = true")

        self.assertEqual(str, type(self._yacp.stuff.param))
        self.assertEqual('true', self._yacp.stuff.param)

        self._yacp.add_section("stuff").add_argument("--param", type=bool)

        self.assertEqual(bool, type(self._yacp.stuff.param))
        self.assertTrue(self._yacp.stuff.param)

    def test_section_redeclare(self):
        section1 = self._yacp.add_section("stuff")
        section2 = self._yacp.add_section("stuff")

        self.assertEqual(section1, section2)
