# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from io import StringIO
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp

from yacp import ArgumentParser


class YacpRootTest(unittest.TestCase):

    def setUp(self):
        self._root_name = "custom-root-name"
        self._yacp = ArgumentParser(root_section_name=self._root_name)
        self._dir = mkdtemp()
        self.addCleanup(rmtree, self._dir)

    def test_write_root_section(self):
        out = StringIO()

        self._yacp.write(out)

        self.assertEqual("[custom-root-name]", out.getvalue().strip())

    def test_read_root_section(self):
        path = Path(self._dir) / "1.ini"
        path.write_text("[custom-root-name]\ntest_param = 42")

        self._yacp.read(path)

        self.assertEqual("42", self._yacp.test_param)

    def test_explicit_root_in_read_string(self):
        self._yacp.read_string("\n".join(["param = 42",
                                          "[custom-root-name]",
                                          "param2 = 15"]))

        self.assertEqual("42", self._yacp.param)
        self.assertEqual("15", self._yacp.param2)
