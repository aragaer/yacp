# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from contextlib import contextmanager
from os import environ

from yacp import ArgumentParser


@contextmanager
def env(key, value):
    environ[key] = value
    try:
        yield
    finally:
        del environ[key]


@env("TEST_PARAM", "42")
class YacpEnvTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()

    def test_env(self):
        self._yacp.add_argument(dest="test_param",
                                env="TEST_PARAM",
                                type=int)

        self.assertEqual(42, self._yacp["test_param"])

    def test_env_overrides_file(self):
        self._yacp.add_argument(dest="test_param",
                                env="TEST_PARAM",
                                type=int)

        self._yacp.read_string("test_param=64")

        self.assertEqual(42, self._yacp["test_param"])

    def test_arg_overrides_env(self):
        self._yacp.add_argument("--test",
                                dest="test_param",
                                env="TEST_PARAM",
                                type=int)

        self._yacp.parse_args(["--test", "64"])

        self.assertEqual(64, self._yacp["test_param"])


class YacpAutoEnvTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser(root_section_name="root",
                                    envvar_prefix="YACP",
                                    envvar_sep="__")

    @env("YACP_TEST_PARAM", "42")
    def test_env(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertEqual(42, self._yacp.test_param)

    @env("YACP_TEST_PARAM__STUFF", "42")
    def test_env_root_beginning(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertIsNone(self._yacp.test_param)

    @env("YACP_ROOT__TEST_PARAM", "42")
    def test_env_root_section(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertEqual(42, self._yacp.test_param)

    @env("YACP_SECTION_NAME__TEST_PARAM", "42")
    def test_env_section(self):
        section = self._yacp.add_section("section-name")
        section.add_argument(dest='test_param', type=int)

        self.assertEqual(42, section.test_param)

    @env("YACP_SECTION_NAME__TEST_PARAM", "42")
    def test_env_no_root(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertIsNone(self._yacp.test_param)

    @env("YACP_TEST_PARAM", "42")
    def test_env_no_section(self):
        section = self._yacp.add_section("section-name")
        section.add_argument(dest='test_param', type=int)

        self.assertIsNone(section.test_param)


class YacpNoAutoEnvTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()

    @env("YACP_TEST_PARAM", "42")
    def test_env(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertIsNone(self._yacp.test_param)

    @env("YACP_ROOT__TEST_PARAM", "42")
    def test_env_root_section(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertIsNone(self._yacp.test_param)

    @env("YACP_SECTION_NAME__TEST_PARAM", "42")
    def test_env_section(self):
        section = self._yacp.add_section("section-name")
        section.add_argument(dest='test_param', type=int)

        self.assertIsNone(section.test_param)


class YacpEmptyAutoEnvTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser(root_section_name="root",
                                    envvar_prefix="",
                                    envvar_sep="__")

    @env("TEST_PARAM", "42")
    def test_env(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertEqual(42, self._yacp.test_param)

    @env("ROOT__TEST_PARAM", "42")
    def test_env_root_section(self):
        self._yacp.add_argument(dest='test_param', type=int)

        self.assertEqual(42, self._yacp.test_param)

    @env("SECTION_NAME__TEST_PARAM", "42")
    def test_env_section(self):
        section = self._yacp.add_section("section-name")
        section.add_argument(dest='test_param', type=int)

        self.assertEqual(42, section.test_param)


class YacpWeirdEnvTest(unittest.TestCase):

    @env("rootXXparam", "42")
    def test_uc_sep(self):
        yacp = ArgumentParser(root_section_name="root",
                              envvar_prefix="",
                              envvar_sep="XX")
        yacp.add_argument("--param", type=int)

        self.assertEqual(42, yacp.param)

    @env("YACP_param", "42")
    def test_disable_env(self):
        yacp = ArgumentParser(envvar_prefix="YACP")

        yacp.add_argument("--param", type=int, env=None)

        self.assertIsNone(yacp.param)
