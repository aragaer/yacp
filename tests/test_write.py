# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from os import environ
from io import StringIO

from yacp import ArgumentParser


class YacpWriteTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()
        self._out = StringIO()

    def test_write_default(self):
        section = self._yacp.add_section("stuff")
        section.add_argument(dest="test_param", type=int, default=42)

        self._yacp.write(self._out)

        self.assertEqual("[yacp-root]\n\n[stuff]\ntest_param = 42",
                         self._out.getvalue().strip())

    def test_not_write_env(self):
        environ["TEST_PARAM"] = "64"

        section = self._yacp.add_section("stuff")
        section.add_argument(dest="test_param", type=int, default=42, env="TEST_PARAM")

        self._yacp.write(self._out)

        self.assertEqual("[yacp-root]\n\n[stuff]\ntest_param = 42",
                         self._out.getvalue().strip())

    def test_not_write_arg(self):
        section = self._yacp.add_section("stuff")
        section.add_argument("--test", dest="test_param", type=int, default=42)

        self._yacp.parse_args(["--test", "64"])

        self._yacp.write(self._out)

        self.assertEqual("[yacp-root]\n\n[stuff]\ntest_param = 42",
                         self._out.getvalue().strip())
