# pylint: disable=missing-module-docstring
# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring

import unittest

from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp

from yacp import ArgumentParser


class YacpFileTest(unittest.TestCase):

    def setUp(self):
        self._yacp = ArgumentParser()
        self._dir = mkdtemp()
        self.addCleanup(rmtree, self._dir)

    def test_read_file(self):
        self._yacp.add_argument(dest="test_param", type=int)

        self._yacp.read_file(["test_param=42"])

        self.assertEqual(42, self._yacp["test_param"])

    def test_read_filename(self):
        path = Path(self._dir) / "1.ini"
        path.write_text("[stuff]\ntest_param = 42")

        section = self._yacp.add_section("stuff")
        section.add_argument(dest="test_param", type=int)

        self._yacp.read(path)

        self.assertEqual(42, section["test_param"])

    def test_read_filenames(self):
        paths = []
        for i in range(5):
            path = Path(self._dir) / f"{i}.ini"
            path.write_text(f"[stuff]\ntest_param = {i+42}")
            paths.append(path)

        section = self._yacp.add_section("stuff")
        section.add_argument(dest="test_param", type=int)

        self._yacp.read(paths)

        self.assertEqual(46, section["test_param"])

    def test_incremental_reading(self):
        finalpath = Path(self._dir) / "final.ini"
        path = finalpath
        for i in range(5):
            newpath = Path(self._dir) / f"{i}.ini"
            newpath.write_text(f"[stuff]\nnext_file = {path}")
            path = newpath

        section = self._yacp.add_section("stuff")
        section.add_argument(dest="next_file")
        section.add_argument("--file", dest="first_file")

        self._yacp.parse_args(["--file", str(path)])

        to_read = section["first_file"]
        while self._yacp.read(to_read):
            to_read = section["next_file"]

        self.assertEqual(str(finalpath), section["next_file"])
